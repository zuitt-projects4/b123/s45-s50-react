import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import bootstrap inn index.js
import 'bootstrap/dist/css/bootstrap.min.css'
/*
  Reactjs - syntax used is JSX.

  JSX - Javascript + XML, It is an extension of Javascript that let's us create objects which will be compiled as HTML Elements.

  With JSX, we are able to create JS objects with HTML like syntax.

  let element = <h1>My First React App!</h1>
  console.log(element);
*/


/*
  Displaying your react elements:

  ReactDOM.render(<reactElement>,<htmlElementSelectedById>)
*/

/*

  Create a new variable called myName and store:
    -an h2 Element with your name.
  Show myName variable instead of element variable.

*/

//with JSX
/*let myName = <h2>Tee Jae Bengan Calinao</h2>*/

//without JSX
/*let myName = React.createElement('h2',{},"This was not created with JSX Syntax");

let person = {

  name: "Stephen Strange",
  age: 45,
  job: "Sorcerer Supreme",
  income: 50000,
  expense: 30000

}

*/


/*let sorcererSupreme = <p>My name is {person.name}. I am {person.age} old. I work as {person.job}</p>*/

/*
  Mini Activity:

  Create a variable and store the following:
   -p tag which has the following message:
    "I am <personName>. I work as <personJob>. My income is <personIncome>. My expense is <personExpense>. My balance is <personBalance>."

*/

/*let sorcererSupreme2 = <p>I am {person.name}. I work as {person.job}. My income is {person.income}. My expense is {person.expense}. My balance is {person.income - person.expense}</p>

  let sorcererSupreme = (

    <>
      <p>My name is {person.name}. I am {person.age} old. I work as {person.job}</p>
      <p>I am {person.name}. I work as {person.job}. My income is {person.income}. My expense is {person.expense}. My balance is {person.income - person.expense}</p>
    </>

    )

*/


/*ReactJS does not like returning 2 elements unless they are wrapped by another element, or what we call a fragment <></>*/

/*
  Components are functions that return react elements.
  components should  start in capital letters 
  Components naming convention(PascalCase)

  how are components be called? <Components/>
  const SampleComp = () =>{

    return (

        <h1>I am returned by a function</h1>

      )
  }

  In react.js, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/ main component.

  All other components/pages will be contained in our main component <App/>

  React.StrictMode is a built in react component which is used to highlight potential problems in the code and in fact allows to provide more information about errors in our code.
*/


ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root'));
