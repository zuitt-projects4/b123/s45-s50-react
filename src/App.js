//When creating a component in a separate file we always import
//require() and import both imports  your modules.
//import react
import React, {useState, useEffect} from 'react'

//import pages
import Home from './pages/Home'
import Courses from './pages/Courses'
import AddCourse from './pages/AddCourse'
import ViewCourse from './pages/ViewCourse'
import Register from './pages/Register'
import Login from './pages/Login'
import NotFound from './pages/NotFound'
import Logout from './pages/Logout'

//import our userProvider
import {UserProvider} from './userContext'

//import router-dom
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'

//import components
import AppNavBar from './components/AppNavBar'

//import container in react-bootstrap
import {Container} from 'react-bootstrap'

//import css
import './App.css'

/*
	Reactjs us a single page application(SPA), however, we can actually simulate the changing of pages. We don't actually create new pages, what we just do is switch pages according to their assigned routes. ReactJS amd react-router-dom packages just mimics or mirrors how HTML accesses it urls.
	

	with this, we don't reload the page each time we switch pages. 

	react-router-dom has 3 main components to stimulate the changing of pages:

	Router
		-wrapping our router containing components 
	
	Switch
		- 
		- allows to switch/change our page components

	Route
		- assigns a path which will trigger the change/switch og components to render


*/

//exports
export default function App(){

	const [user, setUser] = useState({

		id: null,
		isAdmin: null

	})

	useEffect(() => {

		fetch('http://localhost:4000/users/getUserDetails', {

			headers: {
				'Authorization':`Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUser({

				id: data._id,
				isAdmin: data.isAdmin

			})
		})

	}, [])
	//function to clear localStorage on logout		
	const unsetUser =() => {
		//clears content of our localStorage
		localStorage.clear()
	}

	return (
			<>
				<UserProvider value = {{user, setUser, unsetUser}}>
					<Router>
						<AppNavBar/>
						<Container>
							<Switch>
								<Route exact path="/" component={Home} />	
								<Route exact path="/courses" component={Courses} />	
								<Route exact path="/courses/addcourse" component={AddCourse} />	
								<Route exact path="/courses/:courseId" component={ViewCourse} />	
								<Route exact path="/login" component={Login} />	
								<Route exact path="/register" component={Register} />	
								<Route exact path="/logout" component={Logout} />	
								<Route component={NotFound} />	
							</Switch>	
						</Container>
					</Router>
				</UserProvider>	
			</>
		)
}