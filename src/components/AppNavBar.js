import React, {useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import UserContext from '../userContext'

/*
	if your components will have children (such as text-content or other react elements or components), it shoukd have a closing tag. If not, then we stick to self-closing tag.

	classes in reactjs are added as className instead of class.

	"as" prop allows components to be treated as if they are another component. A component with the as prop will be able to gain access to another component's properties and functionalities.

	the useContext hook will allows us to "unwrap" or use our UserContext, get the values passed to it.
*/

export default function AppNavBar(){

	const {user} = useContext(UserContext)
	console.log(user)

	/*console.log(useContext(UserContext))*/

	return(

			<Navbar bg="primary" expand="lg">
				<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link as={Link} to="/">Home</Nav.Link>
						<Nav.Link as={Link} to="/courses">Courses</Nav.Link>
						{
							user.id
							?
								user.isAdmin
								?
								<>
									<Nav.Link as={Link} to="/courses/addCourse"> Add Courses</Nav.Link>
									<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
								</>
								:
								<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={Link} to="/register">Register</Nav.Link>
								<Nav.Link as={Link} to="/login">Login</Nav.Link>
							</>	
						}
					</Nav>
				</Navbar.Collapse>
			</Navbar>

		)

};