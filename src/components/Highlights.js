import React from 'react'
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights() {

	return (

		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Ex sit culpa adipisicing veniam et laborum sit pariatur est anim aliqua deserunt nostrud.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Consectetur mollit sint sunt dolor id laboris amet dolore officia ut ad nulla laborum ad veniam sunt dolore occaecat pariatur commodo consequat dolore ut in nostrud nisi est sed consectetur quis amet laborum ut cupidatat duis aliqua sit dolor.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Dolore amet esse enim in qui reprehenderit ea sunt in irure duis culpa cupidatat reprehenderit ut laborum ut. Sint nulla quis occaecat amet proident reprehenderit deserunt esse deserunt nulla elit.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)

}