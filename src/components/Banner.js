//imports
import React from 'react'
import {Row, Col, Jumbotron} from 'react-bootstrap'
import {Link} from 'react-router-dom'
//Link component creates an anchor tag, however, it does not use href and instead it will just commit to switching our page component instead of reloading.

/*
	Row and Col are components from our react-bootstrap module. They create div elements with bootstrap classes. 

	React-bootstrap components create react elements with bootstrap classes.
*/

//exports
export default function Banner({bannerProp}){


	return(

		<Row>
			<Col>
				<Jumbotron className="text-center">
					<h1>{bannerProp.title}</h1>
					<p>{bannerProp.description}</p>
					<Link to={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonCallToAction}</Link>
				</Jumbotron>
			</Col>
		</Row>

	)

}