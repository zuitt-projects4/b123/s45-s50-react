import React, {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function Course({courseProp}) {
	/*
		states in Reactjs are always to store information within a component. The advantage of a state from a variable is that variables do not retain updated information when the component is updated.

		this console log repeats whenever the enroll button is pressed because whenever a state is updated, the component re-renders.
		
		Rendering in reactjs, rendering is the act of showing our element in the component. It is the run of our component. Whenever our component re-renders, it actually runs the function again, the component runs again.

		Conditional Rendering is when we are able to show/hide elements based on a condition.

		useState(initial) = [counter, setterfunction] is react hook which allows us to create a state and its setter function. useState actually returns 2 items in an array, with the argument in it becoming the initial value of our state variable. we destructure the array returned by useState() and assign them in variable. setterFunction can be named however we want, however, by convention, it should instead be named after what state it is updating.
	*/
	//console.log(useState(0))
	/*

	const [count, setCount] = useState(0)
	const [seats, setSeats] = useState(30)
	const [isActive, setIsActive] = useState(true)
		every time the seats state is updated, we will check if the seats state is equal to 0, and if it is, we will change the value of isActive state to false.
	useEffect(() => {

		if(seats === 0){
			setIsActive(false)
		}

	}, [seats])
	console.log(isActive)

	function enroll(){
		
		setCount(count + 1)
		setSeats(seats - 1) 

	}
	*/
	console.log(courseProp)


	return (

		<Card>
			<Card.Body>
				<Card.Title>{courseProp.name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{courseProp.description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{courseProp.price}</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${courseProp._id}`}>View Course</Link>
			</Card.Body>
		</Card>

	)
}