import React, {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'

export default function Register(){

	const [state1, setState1] = useState(0)
	const [state2, setState2] = useState(0)

	function sample1() {
		setState1(state1 + 1)
	}
	
	function sample2() {
		setState2(state2 + 1)
	}

	useEffect(() =>{

		console.log("I will run on INITIAL RENDER only.")

	}, [])

	return (

		<>
			<Button variant="success" onClick={sample1} className="mx-3 my-5">
				Update State 1
			</Button>
			<Button variant="success" onClick={sample2} className="mx-3 my-5">
				Update State 2
			</Button>
		</>

	)

}

/*
	Review
	Props 
		- is a way to pass data from a parent component to a child component. it is used like an HTML attribute added to rhe child component. the prop then becomes a property iif the special react that all components receive, the props. prop names are user-defined.

	State
		- are a way to store information within a component. this information can then be updated within the component. When a state is updated through its setter function, it will re-render the component. States are contained within their components. they are independent from  other instances of the component.

	Hooks
		- Special/react-defined methods and functions that allows us to do certain tasks in our components.

	useState()
		- is a hook that creates states. useState() returns an array with 2 items. the first item in the array is state and the second item is setter function. we then destructure this returned array and assigned both items in variables.

		const [stateName, setStateName] = useState(initial value of State)

	useEffect() 
		-  is a hook used to create effects. these effects will allows us to run a function or tasks  based on when our effect will run. our useEffect will ALWAYS run on initial render. the next ti,e that the useEffect will run will depend on its dependency array. 

		useEffect(() =>{},[dependency array])

		useEffect() will allow us to run a function or task on initial render and whenever our component re-renders IF there is NO dependency array.

		useEffect will allow us to run  a function or task on initial render only if there is an EMPTY dependency array.

		useEffect will allow us to run  a function or task on initial render AND whenever the state/s in its dependency array is updated.

		useEffect will always run on initial render or the very first time our component is displayed or run.
*/