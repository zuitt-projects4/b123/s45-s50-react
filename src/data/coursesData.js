let coursesData = [

	{
		id: "wdc001", 
		name: "PHP-Laravel",
		description:"Sunt aliqua reprehenderit nostrud ullamco ex consequat in ullamco minim amet esse ad aliquip non.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002", 
		name: "Phython-Django",
		description:"Exercitation pariatur laboris exercitation aute officia in velit nulla consequat est consectetur sed ea cupidatat dolore cupidatat non consectetur.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003", 
		name: "Java-Springboot",
		description:"Sint pariatur esse ut cupidatat excepteur do ut culpa nulla proident reprehenderit nostrud.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc004", 
		name: "Nodejs-MERN",
		description:"Eu enim tempor in incididunt aliquip sunt sed cillum quis sed fugiat irure ex cillum.",
		price: 45000,
		onOffer: false
	}

]

export default coursesData;