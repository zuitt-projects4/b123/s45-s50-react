import React from 'react'
import Banner from '../components/Banner'

export default function NotFound(){

	let notFound = {

		title: "Page Cannot Be Found.",
		description: "A Fun Way to Learn Baking",
		buttonCallToAction: "Back to Home", 
		destination:"/"

	}
	return <Banner bannerProp={notFound}/>
}