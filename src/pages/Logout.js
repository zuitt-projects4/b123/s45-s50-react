import React, {useContext, useEffect} from 'react'
import UserContext from '../userContext'
import Banner from '../components/Banner'

export default function Logout(){

	const {setUser, unsetUser} = useContext(UserContext)

	unsetUser()
	useEffect(() => {

		setUser({

			id:null,
			isAdmin:null

		})

	}, [])

	const bannerContent ={

		title: "See You Later!",
		description: "You have logged out of Sunshine Booking System",
		buttonCallToAction: "Go Back To Home Page",
		destination: "/"


	}
	return(
		
		<Banner bannerProp={bannerContent}/>

	)

}