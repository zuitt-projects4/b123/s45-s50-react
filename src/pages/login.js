//imports
import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../userContext'
import {Redirect} from 'react-router-dom'
/*
	Redirect is a component that will allows us toi redirect our to one of our pages/

*/
//exports
export default function Login() {

	const {user, setUser} = useContext(UserContext)
	
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const [isActive, setIsActive] = useState(false)

	useEffect(() => {

		if(email  !== ""  && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[ email, password])

	function loginUser(e){
		e.preventDefault()
		fetch('http://localhost:4000/users/login', {

			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			}, 
			body: JSON.stringify({

			email: email, 
			password: password, 

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.accessToken) {
				
				Swal.fire({

					icon:"success",
					title:"Login Successful!",
					text: `Thank you for logging in.`

				})

				localStorage.setItem('accessToken', data.accessToken)
				
				fetch('http://localhost:4000/users/getUserDetails', {

					headers: {
						'Authorization':`Bearer ${localStorage.getItem('accessToken')}`
					}
					})
					.then(res => res.json())
					.then(data => {

					setUser({

						id: data._id,
						isAdmin: data.isAdmin

					})
				})

			}else{

				Swal.fire({

					icon:"error",
					title:"Login Failed.",
					text: data.message

				})

			}
		})
	}

	return (
		
		user.id 
		?
		<Redirect to="/courses" />
		:
		<>
			<h1 className="my-5 text-center" >Login</h1>
			<Form onSubmit={e => loginUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" value={email} onChange={e=>{setEmail(e.target.value)}} placeholder="Enter email" required/> 
				</Form.Group>
				<Form.Group controlId="userPassword">
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" value={password} onChange={e=>{setPassword(e.target.value)}} placeholder="Enter Password" required/> 
				</Form.Group>
				{
					isActive
					?<Button variant="primary" type="submit">Submit</Button>
					:<Button variant="primary" disabled>Submit</Button>
				}
			</Form>
		</>

	)
}