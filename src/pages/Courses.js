import React, {useState, useEffect}from 'react'
import coursesData from '../data/coursesData'
import Course from '../components/Course'

export default function Courses() {

	const [courses, setCoursesArray] = useState([])

	useEffect(() =>{

		fetch('http://localhost:4000/courses/getActiveCourses')
		.then(res => res.json())
		.then(data => {

			setCoursesArray(data.map(course =>{
		
			return(

				<Course key={course._id} courseProp={course}/>

				)

			}))
		})
	},[])
	

	console.log(courses)

	return(
			<>
				<h1 className="my-5">Available Courses</h1>
				{courses}
			</>
		)
}
	/*

	the resulting new array from mapping the data(which is an array of course documents) will be set into our coursesArray state with its setter function

	console.log(coursesData)
		Each instance of a component is independent from one another. 
		So if for example, we called multiple course components, each of those instances are independent from each other. Therefore allowing us to have re-usable UI components.

	let sampleProp1 = "I am Sample 1"
	let sampleProp2 = "I am Sample 2"
	*/	
		/*
			reactjs when creating a group/array of react elements, we need to pass a key prop so that reactjs can recognize each individual instances of the react element. Each key prop should be unique.
			let courseCards = coursesData
		*/
