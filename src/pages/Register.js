import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../userContext'
import {Redirect, useHistory} from 'react-router-dom'

export default function Register(){

	const {user, setUser} = useContext(UserContext)

	const history = useHistory()

	//input states
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")

	//conditional rendering for button
	const [isActive, setIsActive] = useState(false)

	/*
		useHistory allows us to use method which will redirect a user to another page without having to reload

		The Two Way Binding

		- in reactjs, we are able to create forms which will allow us to bind the value of our input as the value for our states. we cannot type into our inputs anymore because there is now a value bound to it. We will then add an onChange event per input to be able to update the state with the current value of the input. 

		Two Way Bindin done so that we can ensure that we can save the value of our input in our states. So that we can capture the current value of the input as it is typed on and save in a state as opposed to saving it when we are about to submit the values.


		<Form.Control type="inputType" value={inputState} onChange={e => {setInputState(e.target.value)}}/>

		e = event, all event listeners pass the event object to the function added in the event listener

		e.target = target is the element WHERE the event happened.

		e.target.value = value is a property of target. It is the current value of the element WHERE the event happened.
	*/

	useEffect(() => {

		if((firstName !== "" && lastName !== "" && email  !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[firstName, lastName, email, mobileNo, password, confirmPassword])

	/*	console.log(firstName)
		console.log(lastName)
		console.log(email)
		console.log(mobileNo)
		console.log(password)
		console.log(confirmPassword)*/

	function registerUser(e){

		e.preventDefault()//prevents  submit event's default behavior

		fetch('http://localhost:4000/users/', {

			method: 'POST',
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			// email will only be not undefined if we registered properly.
			if(data.email){

				Swal.fire({

					icon:"success",
					title:"Registration Successful!",
					text: `Thank you for registering. ${data.email}`

				})

				//redirect our user after registering to our login page
				history.push('/login')

			}else{

				Swal.fire({

					icon:"error",
					title:"Registration Failed.",
					text: data.message

				})

			}
		})

	}

	return (

		user.id 
		?
		<Redirect to="/courses" />
		:
		<>
			<h1 className="my-5 text-center" >Register</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" value={firstName} onChange={e=>{setFirstName(e.target.value)}} placeholder="Enter First Name" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" value={lastName} onChange={e=>{setLastName(e.target.value)}} placeholder="Enter Last Name" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" value={email} onChange={e=>{setEmail(e.target.value)}} placeholder="Enter email" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="number" value={mobileNo} onChange={e=>{setMobileNo(e.target.value)}} placeholder="Enter 11 Digit Mobile No." required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" value={password} onChange={e=>{setPassword(e.target.value)}} placeholder="Enter Password" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}} placeholder="Confirm Password" required/> 
				</Form.Group>
				{
					isActive
					?<Button variant="primary" type="submit">Submit</Button>
					:<Button variant="primary" disabled>Submit</Button>
				}
			</Form>
		</>

	)

}