import React, {useState, useEffect, useContext} from 'react'
import UserContext from '../userContext'
import {Form, Button} from 'react-bootstrap'
import UnauthorizedUser from './UnauthorizedUser'
import Swal from 'sweetalert2'
import {Redirect, useHistory} from 'react-router-dom'




export default function AddCourse(){

	const {user} = useContext(UserContext)

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [isActive, setIsActive] = useState(false)

	const history = useHistory()

	useEffect(() => {

		if(name !== "" && description !== "" && price !== "" ){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [name, description, price])

	function createCourse(e){

		e.preventDefault()

		fetch('http://localhost:4000/courses/addCourse',{

			method: 'POST',
			headers: {
				'Content-Type':'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}` 
			}, 
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then( data => {

			console.log(data)

			if(data.message){

				Swal.fire({

					icon:"error",
					title:"Failed to Add Course.",
					text: data.message

				})
				

			}else{

				Swal.fire({

					icon:"success",
					title:"Course Successfully Added!",
					text: `You've added  ${data.name}`

				})

				history.push("/courses")

			}

		})

		setName("")
		setDescription("")
		setPrice("")
			
	}

	return(

		user.isAdmin === false 
		?
		<UnauthorizedUser/>
		:
		<>
			<h1 className="my-5 text-center" >Add Course</h1>
			<Form onSubmit={e => createCourse(e)}>
				<Form.Group>
					<Form.Label>Course Name:</Form.Label>
					<Form.Control type="text" value={name} onChange={e=>{setName(e.target.value)}} placeholder="Enter Course Name" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" value={description} onChange={e=>{setDescription(e.target.value)}} placeholder="Enter Description" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control type="number" value={price} onChange={e=>{setPrice(e.target.value)}} placeholder="Enter price" required/> 
				</Form.Group>
				{
					isActive
					?<Button variant="primary" type="submit">Submit</Button>
					:<Button variant="primary" disabled>Submit</Button>
				}
			</Form>
		</>

	)

}