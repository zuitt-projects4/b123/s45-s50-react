import React, {useState, useEffect, useContext} from 'react'
import {Card, Button, Row, Col} from 'react-bootstrap'
import {useParams, useHistory, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../userContext'


export default function ViewCourse(){

	/*
		useParams() - will allow us to retrieve any parameter in our URL
					- will return an object containing our URL params.
	*/

	const {courseId} = useParams()
	//console.log(courseId)
	const {user} = useContext(UserContext)

	const history = useHistory()

	const [courseDetails, setCourseDetails] = useState({

		name: null,
		description: null, 
		price: null

	})

	useEffect(() => {

		fetch(`http://localhost:4000/courses/getSingleCourses/${courseId}`)
		.then(res => res.json())
		.then(data => {

			if(data.message){

				Swal.fire({

					icon:"error",
					title: "Course Unavailable",
					text: data.message

				})

			}else{

				setCourseDetails({

					name:data.name,
					description: data.description,
					price: data.price

				})

			}

		})

	}, [courseId])

	/*console.log(courseDetails)*/

	const enroll = (courseId) => {

		fetch('http://localhost:4000/users/enroll', {

			method: 'POST',
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body:JSON.stringify({

				courseId: courseId

			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

		})

	}

	return(

		<>
			<Row className="mt-5">
				<Col>
					<Card>
						<Card.Body className="text-center">
						<Card.Title>{courseDetails.name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{courseDetails.description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{courseDetails.price}</Card.Text>
						<Card.Subtitle>Sceduke:</Card.Subtitle>
						<Card.Text>8 am - 5 pm</Card.Text>
						{
							user.isAdmin === false
							?
							<Button variant="primary" block onClick={() => enroll(courseId)}>Enroll</Button>
							:
							<Link className="btn btn-danger btn-block" to="/login">Login to Enroll</Link>
						}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</>

	)
}