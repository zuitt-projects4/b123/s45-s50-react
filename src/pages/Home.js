/*
	Home will be a page component, which will be our pages for our applications.
	ReactJS adheres a D.R.Y. - Don't Repeat Yourself.

	Props  - are data we can pass from a parent component to child component. 

	all components actually are able to receive an object, Props are special react objects with which we can pass data around from a parent to child.
*/
import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

	let samplePop = {

		title: "Sunshine Booking System",
		description: "A Fun Way to Learn Baking.",
		buttonCallToAction: "Start Now", 
		destination:"/login"

	};

	/*
		we can pass props from a parent to child by adding HTML-like attribute which can name ourselves. The name of the attribute will become the property of the object received by all components.

		NOte: Components are independent from each other.
	*/
	return(

		<>
			<Banner bannerProp={samplePop}/>
			<Highlights />
		</>

	)
}